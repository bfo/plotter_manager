#ifndef XMLCOMMANDLISTPARSER_H
#define XMLCOMMANDLISTPARSER_H

#include <QtXml/QDomDocument>
#include <QtXml/QtXml>
#include <QVector>
#include <QIODevice>
#include "commandentry.h"

class XmlCommandListParser
{
public:
  XmlCommandListParser(QIODevice& _xmlFile);
  ~XmlCommandListParser();

  typedef enum {
    OK,
    IO_ERROR,
    PARSE_ERROR,
    WRONG_ROOT_NAME,
    EMPTY_ROOT_TAG,
    MISSING_COMMAND_ID,
    WRONG_COMMAND_ID,
    EMPTY_COMMAND_TAG,
    MISSING_NAME_TAG,
    MISSING_TYPE_TAG,
    MISSING_REQ_RESP,
    WRONG_TYPE_VALUE,
    WRONG_MINDB_VALUE,
    WRONG_MAXDB_VALUE
  } ParseStatus;

  typedef struct {
    QString errMsg;
    int line;
    int column;
  } ParseErrorInfo;

  ParseStatus parse();
  ParseErrorInfo getLastError() const;
  QVector<CommandEntry*> getCommands() const;

private:
  QDomDocument document;
  QVector<CommandEntry*> commands;
  QIODevice* xmlFile;
  ParseErrorInfo errInfoStruct;

  ParseStatus parseCommandEntry(QDomElement& element, CommandEntry& cmd);
  void clearActionsMemory();


};

#endif // XMLCOMMANDLISTPARSER_H
