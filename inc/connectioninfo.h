#ifndef CONNECTIONINFO_H
#define CONNECTIONINFO_H

#include <QString>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QQueue>
#include <QMutex>
#include <QTimer>
#include <pdu.h>

class ConnectionInfo : public QObject
{
  Q_OBJECT
public:
  ConnectionInfo();
  ~ConnectionInfo();


  // management part
  typedef enum {
    OK,
    ERR_IO,
    ERR_ORDER,
    ERR_SETTING_PARAMS
  } ErrorCode;

  typedef enum {
    NEED_INITIALIZING,
    READY_TO_OPEN,
    OPEN,
    SETTING_PARAMS,
    ERR_OPENING,
    ERR_CLOSING
  } ConnectionStatus;

  bool isOpen() const;
  ConnectionStatus getStatus() const;
  QString getErrMsg() const;


private slots:
  void doWork();

public slots:
  // Command API
  void initialize(const QSerialPortInfo&);
  void addToQueue(const PDU* packet);
  bool isQueueEmpty();

  // Control API
  void scheduleAbort();
  bool getAbortScheduled();
  void pauseTransmission();
  void resumeTransmission();
  bool isTransmissionResumed();

signals:
  void byteSent();//used to inform caller that one byte of data has been successfully transferred
  void PDUSent();//used to inform caller that one packet has been successfully transferred
  void transmissionCompleted();//used to inform caller that transmission has been correctly terminated
  void transmissionError(ConnectionInfo::ErrorCode code);//emitted every time an error occurs
  void transmissionFatalError(ConnectionInfo::ErrorCode code);//emitted on a non-recoverable error
  void freedResources();
  void transmissionInitialized();
  void terminating();
//  void finished();//used to inform caller that this thread is ready to be scheduled to terminate


private:
  ConnectionInfo(const ConnectionInfo&);
  ConnectionInfo& operator=(const ConnectionInfo&);

  void setError(ConnectionInfo::ErrorCode);
  ErrorCode openPort();
  ErrorCode send(const PDU& _payload);

  const QSerialPortInfo* portInfo;
  QSerialPort* sp;
  QQueue<const PDU*> packetsQueue;
  QMutex queueMutex;
  const PDU* popFromQueue();


  ConnectionStatus status;
  ErrorCode error;

  //Control fields
  bool abortScheduled;
  QMutex abortScheduledMutex;
  bool transmissionStopped;
  QMutex transmissionStoppedMutex;

  QTimer timer;

};

#endif // CONNECTIONINFO_H
