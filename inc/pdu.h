#ifndef PDU_H
#define PDU_H

#include <QtGlobal>
#include <QByteArray>

class PDU
{
public:
  PDU();
  PDU(const QByteArray& _payload);

  quint8 getAddress() const;
  void setAddress(const quint8 _address);

  quint8 getCommand() const;
  void setCommand(const quint8 _command);

  bool getRequestResponse() const;
  void setRequestResponse(const bool bit);

  quint8 getNumberOfParameters() const;

  QByteArray getParameters() const;
  void removeAllParameters();
  void addParameter(const quint8 _parameter);

  quint8 getCrc8() const;
  void computeCrc8();

  QByteArray getFrame() const;

private:
  typedef struct {
    quint8 address;
    quint8 command;
    QByteArray parameters;
    quint8 crc8;
  } Packet;
  Packet payload;

  bool isResponseNeeded;
};

#endif // PDU_H
