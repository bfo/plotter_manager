#ifndef COMMANDENTRY_H
#define COMMANDENTRY_H

#include <QString>

class CommandEntry
{
public:
    CommandEntry();

    typedef enum {
      BROADCAST = 1,
      UNICAST = 2,
      ALL
    } CommandType;

    int getID() const;
    void setID(int id);

    QString getName() const;
    void setName(QString name);

    CommandType getType() const;
    void setType(CommandType type);

    int getMaxDataBytes() const;
    void setMaxDataBytes(int n);

    int getMinDataBytes() const;
    void setMinDataBytes(int n);

    bool getResponseNeeded() const;
    void setResponseNeeded(bool value);

private:
    int id;
    QString name;
    CommandType type;
    int maxDataBytes;
    int minDataBytes;
    bool responseNeeded;
};

#endif // COMMANDENTRY_H
