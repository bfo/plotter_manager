#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <pdu.h>
#include <connectioninfo.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btConnect_released();

    void on_btRefreshPorts_released();

    void on_btDisconnect_released();

    void on_btExecuteCommand_released();

    void onTransmissionError(ConnectionInfo::ErrorCode err);

    void onPDUSent();

    void onConnectionTerminated();
    void ui_prepareAfterConnectionEstablished();
    void ui_prepareAfterConnectionTerminated();

    void commThreadStarted();

signals:
    void do_scheduleAbort();
    void do_addToQueue(const PDU* packet);
    void initCommunication(const QSerialPortInfo&);

private:
    Ui::MainWindow *ui;

    void fillInPorts();
    PDU* constructFrame();

    QThread* communicationThread;
    ConnectionInfo* connectionInfoInstance;
};

#endif // MAINWINDOW_H
