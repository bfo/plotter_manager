#-------------------------------------------------
#
# Project created by QtCreator 2013-04-30T15:48:00
#
#-------------------------------------------------

QT       += xml core gui
CONFIG       += qt serialport

#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets xml
SRCDIR = src
INCDIR = inc
RESDIR = resources

TARGET = PlotManager
TEMPLATE = app


SOURCES += $${SRCDIR}/main.cpp\
        $${SRCDIR}/mainwindow.cpp \
    $${SRCDIR}/connectioninfo.cpp \
    $${SRCDIR}/commandentry.cpp \
    $${SRCDIR}/xmlcommandlistparser.cpp \
    src/pdu.cpp

HEADERS  += $${INCDIR}/mainwindow.h \
    $${INCDIR}/connectioninfo.h \
    $${INCDIR}/commandentry.h \
    $${INCDIR}/xmlcommandlistparser.h \
    inc/pdu.h

INCLUDEPATH += inc/

FORMS    += $${RESDIR}/mainwindow.ui

OTHER_FILES +=

RESOURCES += \
    resources.qrc
