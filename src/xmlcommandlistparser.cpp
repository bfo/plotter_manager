#include "xmlcommandlistparser.h"
#include <QtXml/QDomDocument>
#include <QtXml/QDomElement>

XmlCommandListParser::XmlCommandListParser(QIODevice& _xmlFile) :
  xmlFile(&_xmlFile)
{
}

XmlCommandListParser::~XmlCommandListParser()
{
  this->clearActionsMemory();
}

XmlCommandListParser::ParseStatus XmlCommandListParser::parse()
{
  if (!this->document.setContent(this->xmlFile,false,&(this->errInfoStruct.errMsg),&(this->errInfoStruct.line),&(this->errInfoStruct.column)))
  {
    return PARSE_ERROR;
  }

  // get handle to root element, which should be <commands>
  QDomElement root = this->document.documentElement();
  if (root.tagName() != "commands")
  {
    return WRONG_ROOT_NAME;
  }

  if (!root.hasChildNodes())
  {
    return EMPTY_ROOT_TAG;
  }

  //clear the command vector
  this->clearActionsMemory();

  QDomElement cmdTag;
  ParseStatus retcode;
  for(uint i = 0; i < root.childNodes().length(); i++)
  {
    cmdTag = root.childNodes().at(i).toElement();
    commands.append(new CommandEntry);
    retcode = parseCommandEntry(cmdTag, *(commands.last()));
    if (retcode != OK)
    {
      this->clearActionsMemory();
      return retcode;
    }
  }

  return OK;
}

XmlCommandListParser::ParseErrorInfo XmlCommandListParser::getLastError() const
{
  return this->errInfoStruct;
}

QVector<CommandEntry*> XmlCommandListParser::getCommands() const
{
  return this->commands;
}


XmlCommandListParser::ParseStatus XmlCommandListParser::parseCommandEntry(QDomElement& element, CommandEntry& cmd)
{
  QDomNodeList commandTagContent = element.childNodes();
  bool typeTagFound = false;
  bool nameTagFound = false;
  bool minDataBytesTagFound = false;
  bool maxDataBytesTagFound = false;

  QDomElement nameTag, typeTag, minDataBytesTag, maxDataBytesTag;
  /*
    Mandatory attrs:
      command#id
      command#req_resp
  */
  if (!element.hasAttribute("id")) {
    return MISSING_COMMAND_ID;
  }
  if (!element.hasAttribute("req_resp")) {
    return MISSING_REQ_RESP;
  }



  /*
    Mandatory fields:
      command/type
      command/name

    Optional fields:
      command/max_data_bytes
      command/min_data_bytes

  */
  if (!element.hasChildNodes()) return EMPTY_COMMAND_TAG;

  QDomElement tmp;
  //traverse list of children
  for( uint i = 0;i < commandTagContent.length();i++)
  {
    tmp = element.childNodes().at(i).toElement();
    if (tmp.tagName() == "name")
    {
      nameTagFound = true;
      nameTag = tmp;
    }
    else if (tmp.tagName() == "type")
    {
      typeTagFound = true;
      typeTag = tmp;
    }
    else if (tmp.tagName() == "min_data_bytes")
    {
      minDataBytesTagFound = true;
      minDataBytesTag = tmp;
    }
    else if (tmp.tagName() == "max_data_bytes")
    {
      maxDataBytesTagFound = true;
      maxDataBytesTag = tmp;
    }
  }

  if (!nameTagFound) return MISSING_NAME_TAG;
  if (!typeTagFound) return MISSING_TYPE_TAG;
  else
  {
    if (typeTag.text() != "unicast" && typeTag.text() != "broadcast") return WRONG_TYPE_VALUE;
  }
  if (minDataBytesTagFound && (minDataBytesTag.text().toInt() < 0 || minDataBytesTag.text().toInt() > 8)) return WRONG_MINDB_VALUE;
  if (maxDataBytesTagFound && (maxDataBytesTag.text().toInt() < 0 || maxDataBytesTag.text().toInt() > 8)) return WRONG_MAXDB_VALUE;

  /********** From this moment on <command> element has been successfully parsed **************/

  // Set name
  cmd.setName(nameTag.text());
  //Set ID
  cmd.setID(element.attribute("id").toInt());
  //Set REQ_RESP bit
  if( "true" == element.attribute("req_resp").toLower() || 1 == element.attribute("req_resp").toInt() ){
    cmd.setResponseNeeded(true);
  } else {
    cmd.setResponseNeeded(false);
  }
  //Set type
  if (typeTag.text() == "unicast") cmd.setType(CommandEntry::UNICAST);
  else cmd.setType(CommandEntry::BROADCAST);
  //Set min and max data bytes if they exist
  if (minDataBytesTagFound) cmd.setMinDataBytes(minDataBytesTag.text().toInt());
  if (maxDataBytesTagFound) cmd.setMaxDataBytes(maxDataBytesTag.text().toInt());

  /********** Structure filled, everything clicks ********************/
  return OK;
}

void XmlCommandListParser::clearActionsMemory()
{
  foreach(CommandEntry* cmd, commands)
  {
    delete cmd;
  }
  commands.clear();
}
