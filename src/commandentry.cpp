#include "commandentry.h"
#include <QString>

CommandEntry::CommandEntry() :
  id(-1),name(""),type(UNICAST),maxDataBytes(0),minDataBytes(0),responseNeeded(false)
{
}

int CommandEntry::getID() const
{
  return this->id;
}

void CommandEntry::setID(int id)
{
  this->id = id;
}

QString CommandEntry::getName() const
{
  return this->name;
}

void CommandEntry::setName(QString name)
{
  this->name = name;
}

CommandEntry::CommandType CommandEntry::getType() const
{
  return this->type;
}


void CommandEntry::setType(CommandEntry::CommandType type)
{
  this->type = type;
}

int CommandEntry::getMaxDataBytes() const
{
  return this->maxDataBytes;
}

void CommandEntry::setMaxDataBytes(int n)
{
  this->maxDataBytes = n;
}

int CommandEntry::getMinDataBytes() const
{
  return this->minDataBytes;
}

void CommandEntry::setMinDataBytes(int n)
{
  this->minDataBytes = n;
}

bool CommandEntry::getResponseNeeded() const
{
  return responseNeeded;
}

void CommandEntry::setResponseNeeded(bool value)
{
  responseNeeded = value;
}
