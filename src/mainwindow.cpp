#include <inc/mainwindow.h>
#include <ui_mainwindow.h>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QDebug>
#include <QFile>
#include <QMessageBox>



#include "inc/xmlcommandlistparser.h"
#include "inc/commandentry.h"
#include "inc/connectioninfo.h"
#include "inc/commandentry.h"
#include <pdu.h>

Q_DECLARE_METATYPE(QSerialPortInfo)
Q_DECLARE_METATYPE(CommandEntry)

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  communicationThread(0)
{
  ui->setupUi(this);


  //Register QSerialPortInfo and CommandEntry in order to be able to pack it into QVariant
  qRegisterMetaType<QSerialPortInfo>("QSerialPortInfo");
  qRegisterMetaType<CommandEntry>("CommandEntry");


  //connect all signals to slots
  //  connect(ui->btConnect,SIGNAL(released()),this,SLOT(on_btConnect_released()));
  //  connect(ui->btRefreshPorts, SIGNAL(released()), this, SLOT(on_btRefreshPorts_released()));



  //fill in the available serial devices
  fillInPorts();


  // try to parse XML file with commands
  QFile commandXML("commands.xml");
  if (!commandXML.open(QIODevice::ReadOnly))
  {
    QMessageBox::information(window(),"Plotter Manager",tr("Something went wrong opening file"));
  } else {
    XmlCommandListParser xmlparser(commandXML);
    if (xmlparser.parse() != XmlCommandListParser::OK)
    {
      QMessageBox::information(window(),"Plotter Manager", tr("XML could not be parsed!"));
    } else {
      // load Vector into QVariant and fill the ComboBox
      QVector<CommandEntry*> cbContent = xmlparser.getCommands();
      QVariant cbAdditionalInfo;
      foreach (CommandEntry* cmdEntry, cbContent) {
        cbAdditionalInfo.setValue<CommandEntry>(*cmdEntry);
        this->ui->cbCommands->addItem(QString("%1 : %2").arg(cmdEntry->getID()).arg(cmdEntry->getName()),cbAdditionalInfo);
      }
    }
  }
}

MainWindow::~MainWindow()
{
  delete ui;
}


void MainWindow::fillInPorts(){
  QString cbEntry;
  QVariant cbAdditionalInfo;
  qDebug() << "Available serial ports:" << QSerialPortInfo::availablePorts().size();
  if(QSerialPortInfo::availablePorts().size() > 0)
  {
    this->ui->btConnect->setEnabled(true);
  } else {
    this->ui->btConnect->setEnabled(false);
  }
  foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
    qDebug() << "Serial port found: ";
    qDebug() << "\tName :" << info.portName();
    qDebug() << "\tDesc  :" << info.description();
    cbEntry = "Nazwa: " + info.portName() + "\nOpis: " + info.description();
    //pack informational structure into combo box, so that we can open a given port at a later time
    cbAdditionalInfo.setValue<QSerialPortInfo>(info);

    this->ui->cbDevices->addItem(cbEntry,cbAdditionalInfo);
  }

}

void MainWindow::ui_prepareAfterConnectionEstablished(){

  this->ui->cbDevices->setEnabled(false);
  this->ui->btRefreshPorts->setEnabled(false);
  this->ui->btDisconnect->setEnabled(true);
  this->ui->btConnect->setEnabled(false);
  this->ui->gbCommand->setEnabled(true);
  this->ui->gbRun->setEnabled(true);
}

void MainWindow::ui_prepareAfterConnectionTerminated()
{
  this->ui->btConnect->setEnabled(true);
  this->ui->btDisconnect->setEnabled(false);
  this->ui->cbDevices->setEnabled(true);
  this->ui->btRefreshPorts->setEnabled(true);
  this->ui->gbCommand->setEnabled(false);
  this->ui->gbRun->setEnabled(false);
}

void MainWindow::commThreadStarted()
{
  //get handle to combobox
  QComboBox* ports = this->ui->cbDevices;
  //obtain handle to port info
  const QSerialPortInfo& serialInfo = ports->itemData(ports->currentIndex()).value<QSerialPortInfo>();

  //initialize communication
  qDebug() << "Trying to connect to serial port...";
  emit this->initCommunication(serialInfo);
}

PDU* MainWindow::constructFrame()
{
  CommandEntry commandEntry = this->ui->cbCommands->itemData(this->ui->cbCommands->currentIndex()).value<CommandEntry>();
  QStringList params;
  //Pick out arguments from semicolon-separated values
  if( this->ui->leParams->text() != "" )
    params = this->ui->leParams->text().split(';') ;

  PDU* packet = new PDU;
  //Construct frame:
  //[1] address
  packet->setAddress( (quint8)this->ui->sbAddress->text().toUInt());
  packet->setRequestResponse(commandEntry.getResponseNeeded());
  //[2] command
  packet->setCommand( (quint8)commandEntry.getID() );
  //[4-11] param
  foreach(QString param,params){
    packet->addParameter((quint8)param.toUInt(0,16) );
  }
  //[12] crc8
  packet->computeCrc8();
  return packet;
}

void MainWindow::on_btExecuteCommand_released(){
  PDU* packet = this->constructFrame();
  qDebug() << "Bytes to be written: " << packet->getFrame().toHex();
  //Payload ready - send it
  //By pushing packet pointer onto the queue, we retain information about the allocated memory
  //Further management is done inside the communication library
  emit do_addToQueue(packet);
}

void MainWindow::on_btConnect_released()
{
  connectionInfoInstance = new ConnectionInfo;
  //create thread and initialize communication
  communicationThread = new QThread;
  qDebug() << communicationThread;
  //run all the slots in the slave thread
  connectionInfoInstance->moveToThread(communicationThread);
  //inform the main thread when it has started
  connect(communicationThread,SIGNAL(started()),this,SLOT(commThreadStarted()));
  //enable the main thread to start communication in the slave thread
  connect(this,SIGNAL(initCommunication(QSerialPortInfo)),connectionInfoInstance,SLOT(initialize(QSerialPortInfo)));
  //when error occurs, inform the user
  connect(connectionInfoInstance,SIGNAL(transmissionError(ConnectionInfo::ErrorCode)),this,SLOT(onTransmissionError(ConnectionInfo::ErrorCode)));
  //when fatal error occurs, also inform the user and prepare the ui
  connect(connectionInfoInstance,SIGNAL(transmissionFatalError(ConnectionInfo::ErrorCode)),this,SLOT(onTransmissionError(ConnectionInfo::ErrorCode)));
  //when transmission ends successfully, inform the user
  connect(connectionInfoInstance,SIGNAL(transmissionCompleted()),this,SLOT(onConnectionTerminated()));
  //when a frame has been sent, inform the user
  connect(connectionInfoInstance,SIGNAL(PDUSent()),this,SLOT(onPDUSent()));
  //enable the main thread to abort the communication
  connect(this,SIGNAL(do_scheduleAbort()),connectionInfoInstance,SLOT(scheduleAbort()),Qt::DirectConnection);
  //enable the main thread to add messages to the queue
  connect(this,SIGNAL(do_addToQueue(const PDU*)),connectionInfoInstance,SLOT(addToQueue(const PDU*)));
  //make changes to the GUI when a connection has been established
  connect(connectionInfoInstance,SIGNAL(transmissionInitialized()),this,SLOT(ui_prepareAfterConnectionEstablished()));
  //make changes to GUI when a thread has been destroyed
  connect(connectionInfoInstance,SIGNAL(terminating()),this,SLOT(ui_prepareAfterConnectionTerminated()));

  //start the thread
  communicationThread->start();

}

//SLOTS
void MainWindow::onConnectionTerminated(){
  connectionInfoInstance = 0;
  communicationThread = 0;
  qDebug() << "Successfully disconnected from port.";
  //prepare UI
  QMessageBox::information(0,"Plot Manager - communication thread", "Communication ended", QMessageBox::Ok);
}

void MainWindow::onPDUSent(){
  QMessageBox::information(0,"Plot Manager - communication thread","Packet sent successfully",QMessageBox::Ok);
}

void MainWindow::onTransmissionError(ConnectionInfo::ErrorCode err){
  QMessageBox::critical(0,"Plot Manager - communication thread", QString("There was a failure %1").arg(err), QMessageBox::Ok);
}

void MainWindow::on_btRefreshPorts_released()
{
  qDebug() << "Refreshing serial ports...";
  this->ui->cbDevices->clear();
  fillInPorts();
}

void MainWindow::on_btDisconnect_released()
{
  //get handle to config object
  emit do_scheduleAbort();
}
