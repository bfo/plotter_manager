#include "connectioninfo.h"
#include <QtSerialPort/QSerialPort>
#include <QDebug>
#include <QThread>
#include <QTimer>

Q_DECLARE_METATYPE(ConnectionInfo::ErrorCode)
Q_DECLARE_METATYPE(QSerialPortInfo)

ConnectionInfo::ConnectionInfo():status(NEED_INITIALIZING),error(OK),abortScheduled(false),transmissionStopped(false){
  qRegisterMetaType<ConnectionInfo::ErrorCode>("ConnectionInfo::ErrorCode");
  qRegisterMetaType<QSerialPortInfo>("QSerialPortInfo");
}

ConnectionInfo::~ConnectionInfo()
{
  emit terminating();
  //drop any remaining packets
  while(! this->packetsQueue.empty()){
    delete(this->packetsQueue.dequeue());
  }

  if (this->sp->isOpen()) {
    qDebug() << "Closing port " << this->portInfo->systemLocation().toUtf8();
    this->sp->close();
  }
  if (this->status >= OPEN) {
    delete this->portInfo;
    this->portInfo = 0;
  }
  delete this->sp;
  qDebug() << "Cleaned resources";
  emit freedResources();
}

bool ConnectionInfo::isOpen() const
{
  return this->status == OPEN;
}

ConnectionInfo::ConnectionStatus ConnectionInfo::getStatus() const
{
  return status;
}

QString ConnectionInfo::getErrMsg() const
{
  return this->sp->errorString();
}

void ConnectionInfo::initialize(const QSerialPortInfo& serialInfo)
{
  connect(this,SIGNAL(transmissionFatalError(ConnectionInfo::ErrorCode)),this->thread(),SLOT(quit()));
  connect(this,SIGNAL(transmissionCompleted()),this->thread(),SLOT(quit()));
  connect(this->thread(),SIGNAL(finished()),this->thread(),SLOT(deleteLater()));
  connect(this->thread(),SIGNAL(finished()),this,SLOT(deleteLater()));
  bool fail = false;
  if (this->status != NEED_INITIALIZING) {
    emit transmissionFatalError(ERR_ORDER);
    return;
  } else {
    this->portInfo = new QSerialPortInfo(serialInfo);
    this->sp = new QSerialPort();
    this->sp->setPort(*(this->portInfo));
    this->status = READY_TO_OPEN;
    if( OK != this->openPort() ){
      //could not open port, so delete info memory and terminate singalize the thread termination
      delete this->portInfo;
      delete this->sp;
      this->portInfo = 0;
      emit transmissionFatalError(ERR_IO);
      return;
    }
    this->status = SETTING_PARAMS;
    if( !(this->sp->setBaudRate(QSerialPort::Baud9600)) ) fail = true;
    if( !(this->sp->setDataBits(QSerialPort::Data8)) ) fail = true;
    if( !(this->sp->setStopBits(QSerialPort::OneStop)) ) fail = true;
    if( !(this->sp->setFlowControl(QSerialPort::HardwareControl)) ) fail = true;
    if(fail){
      delete this->portInfo;
      delete this->sp;
      this->portInfo = 0;
      emit transmissionFatalError(ERR_SETTING_PARAMS);
      return;
    }
    this->status = OPEN;
    emit transmissionInitialized();
  }
  qDebug() << "Let's get moving";
  if(this->status == NEED_INITIALIZING) {
    emit transmissionError(ERR_ORDER);
    return;
  }
  doWork();
}

void ConnectionInfo::doWork()
{
  const PDU* current_packet = 0;
  ErrorCode current_transmission_error = OK;
  if( this->getAbortScheduled() ) {
    qDebug() << "Packing up the stuff";
    emit transmissionCompleted();
    return;
  }
  if(!this->isTransmissionResumed()) {
    QTimer::singleShot(10,this,SLOT(doWork()));
    return;
  }
  if( !this->isQueueEmpty() ){
    current_packet = this->popFromQueue();
    qDebug() << "I see packet";
    if(OK != (current_transmission_error = this->send(*current_packet)) ){
      //Something went wrong, stop transmission and send an error signal
      this->pauseTransmission();
      emit transmissionError(current_transmission_error);
    } else {
      delete current_packet;
    }
    current_packet = 0;
  }
  QTimer::singleShot(10,this,SLOT(doWork()));
}

ConnectionInfo::ErrorCode ConnectionInfo::openPort()
{
  qDebug() << "Otwieram port";
  if (this->status != READY_TO_OPEN) {
    return ERR_ORDER;
  } else {
    //something went wrong, try opening port again OR just open the port
    if (this->sp->open(QIODevice::WriteOnly)) {
      this->status = OPEN;
      return OK;
    } else {
      return ERR_IO;
    }
  }
}

ConnectionInfo::ErrorCode ConnectionInfo::send(const PDU& _payload)
{
  qDebug() << "Sending packet";
  QByteArray raw_frame = _payload.getFrame();
  if(raw_frame.length() == 0) return ERR_IO;
  char addressByte = raw_frame.at(0);
  if( !(this->sp->setParity(QSerialPort::MarkParity)) ) return ERR_SETTING_PARAMS;
  if( this->sp->write((const char*)&addressByte,1) != 1 ){//an error occured
    return ERR_IO;
  }
  this->sp->waitForBytesWritten(-1);
  emit byteSent();
  if( !(this->sp->setParity(QSerialPort::SpaceParity)) ) return ERR_SETTING_PARAMS;
  raw_frame.remove(0,1);//remove address byte from payload
  quint8 byte = 0;
  for(quint8 i = 0; i < raw_frame.length(); i++){
    byte = raw_frame.at(i);
    if( (this->sp->write((const char*)&byte,1)) != 1) {
      return ERR_IO;
    }
    emit byteSent();
  }
  this->sp->waitForBytesWritten(-1);
  emit PDUSent();

  qDebug() << "Sent packet";
  return OK;
}

const PDU*ConnectionInfo::popFromQueue()
{
  QMutexLocker locker(&queueMutex);
  return this->packetsQueue.dequeue();
}


bool ConnectionInfo::isQueueEmpty()
{
  QMutexLocker locker(&queueMutex);
  return packetsQueue.empty();
}

void ConnectionInfo::scheduleAbort()
{
  qDebug() << "Someone wants me to close!";
  QMutexLocker locker(&abortScheduledMutex);
  this->abortScheduled = true;
}

bool ConnectionInfo::getAbortScheduled()
{
  QMutexLocker locker(&abortScheduledMutex);
  return this->abortScheduled;
}

void ConnectionInfo::pauseTransmission()
{
  QMutexLocker locker(&transmissionStoppedMutex);
  this->transmissionStopped = true;
}

void ConnectionInfo::resumeTransmission()
{
  QMutexLocker locker(&transmissionStoppedMutex);
  this->transmissionStopped = false;
}

bool ConnectionInfo::isTransmissionResumed()
{
  QMutexLocker locker(&transmissionStoppedMutex);
  return !(this->transmissionStopped);
}

void ConnectionInfo::addToQueue(const PDU* packet)
{
  QMutexLocker locker(&queueMutex);
  this->packetsQueue.enqueue(packet);
}


