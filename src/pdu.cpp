#include "pdu.h"

PDU::PDU():isResponseNeeded(false){}

PDU::PDU(const QByteArray& _payload)
{
  if(_payload.size() < 4) return;
  if(_payload.size() - _payload.at(2) != 4) return;
  this->payload.address = _payload.at(0);
  this->payload.command = _payload.at(1);
  int i = 0;
  for(i=0; i < _payload.at(2); i++){
    this->payload.parameters.append(_payload.at(3+i));
  }
  this->payload.crc8 = _payload.at(3+i);
}

quint8 PDU::getAddress() const
{
  return this->payload.address;
}

void PDU::setAddress(quint8 _address)
{
  this->payload.address = _address;
}

quint8 PDU::getCommand() const
{
  return this->payload.command;
}

void PDU::setCommand(const quint8 _command)
{
  this->payload.command = _command;
}

bool PDU::getRequestResponse() const
{
  return this->isResponseNeeded;
}

void PDU::setRequestResponse(const bool bit)
{
  this->isResponseNeeded = bit;
}

quint8 PDU::getCrc8() const
{
  return this->payload.crc8;
}

void PDU::computeCrc8()
{
  QByteArray _payload = this->getFrame();
  _payload.chop(1); //Remove CRC, which is for sure wrong at this point
  quint8 bit_counter, feedback_bit, crc8_result = 0;
  foreach(quint8 byte,_payload)
  {
    for (bit_counter = 8; bit_counter; bit_counter--)
    {
      feedback_bit = (crc8_result & 0x01);
      crc8_result >>= 1;
      if (feedback_bit ^ (byte & 0x01))
      {
        crc8_result ^= 0x8c;
      }
      byte >>= 1;
    }
  }
  this->payload.crc8 = crc8_result;
}

QByteArray PDU::getFrame() const
{
  QByteArray _payload;
  _payload.append((this->payload.address << 1) | (quint8)this->isResponseNeeded );
  _payload.append(this->payload.command);
  _payload.append(this->payload.parameters.size());
  _payload.append(this->payload.parameters);
  _payload.append(this->payload.crc8);
  return _payload;
}

void PDU::addParameter(const quint8 _parameter)
{
  this->payload.parameters.append(_parameter);
}


void PDU::removeAllParameters()
{
  this->payload.parameters.clear();
}


QByteArray PDU::getParameters() const
{
  return this->payload.parameters;
}


quint8 PDU::getNumberOfParameters() const
{
  return this->payload.parameters.size();
}
